<?php
/**
 * Additional site settings.
 */

// Forcibly disable poorman's cron.
$conf['cron_safe_threshold'] = 0;

// Database connection settings.
$databases = array (
  'default' =>
  array (
    'default' =>
    array (
      'database' => 'yo_p2_drone_drupal',
      'username' => 'admin',
      'password' => 'admin',
      // @TODO: no dnsdock
      'host' => '127.0.0.1',
      'port' => '',
      'driver' => 'mysql',
      'prefix' => '',
    ),
  ),
);

// @TODO: Need to add some default logic for enabling memcache support
// // Add Memcache for internal caching.
// $conf['cache_backends'][] = 'sites/all/modules/contrib/memcache/memcache.inc';
// $conf['cache_default_class'] = 'MemCacheDrupal';
// $conf['cache_class_cache_form'] = 'DrupalDatabaseCache';
// $conf['lock_inc'] = 'sites/all/modules/contrib/memcache/memcache-lock.inc';
// // Good in theory but stampede protection causes performance regressions.
// // Explicitly setting the default behavior in caution for testing.
// $conf['memcache_stampede_protection'] = FALSE;
// $conf['memcache_servers'] = array(
//   'cache:11211' => 'default',
// );
// $conf['memcache_bins'] = array(
//  'cache' => 'default',
// );
// $conf['memcache_key_prefix'] = 'yo_p2_drone_';

// Include local settings file as an override.
// settings.local.php should not be committed to the Git repository.
if (file_exists(__DIR__ . '/settings.local.php')) {
  include __DIR__ . '/settings.local.php';
}
