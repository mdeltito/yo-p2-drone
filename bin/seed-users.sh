#!/usr/bin/env bash
##
# Seed Users
#
# Creates dummy users for testing.
#
# Run from root of the code repository.
#
# This script is not automatically triggered by Grunt, and must be run/automated
# separately if desired in a given environment.
##

drush @yo-p2-drone user-create "yo-p2-droneadmin" --password="admin1" --mail="yo-p2-droneadmin@example.com"
drush @yo-p2-drone user-add-role "administrator" "yo-p2-droneadmin"
