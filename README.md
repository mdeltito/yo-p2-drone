# Yo P2 Drone

[![Build Status](http://drone.mdel.io/api/badges/mdeltito/yo-p2-drone/status.svg)](http://drone.mdel.io/mdeltito/yo-p2-drone)

> Example of a yo-p2 generated project running on drone

## Drone Test Commit Log

- Test 1
- Test 2
- Test 3
- Test 4
- Test 5
- Test 6
- Test 7
- Test 8

## Screenshots

- [Drone build list](https://dl.dropboxusercontent.com/s/19c7ju70nhu085x/2016-04-30%20at%202.45%20AM.png)
- [Bitbucket build status](https://dl.dropboxusercontent.com/s/m48hqcvvib3g7qb/2016-04-30%20at%202.23%20AM.png)
- [Bitbucket build detail](https://dl.dropboxusercontent.com/s/a9yhfzo91xnt6m9/2016-04-30%20at%202.25%20AM.png)

This project is built directly on Drupal Core, it is not leveraging other distributions. For more information visit the [Drupal Project homepage](http://drupal.org/project/drupal).

## Requirements

* [Node.js](https://nodejs.com) v4.3.x via a [package manager](https://github.com/joyent/node/wiki/Installing-Node.js-via-package-manager) or [standalone installer](http://nodejs.org/download/)
* [Grunt](https://gruntjs.org) (`npm install -g grunt-cli`)
* PHP 5.4
* [Composer](https://getcomposer.org/download) (e.g. `brew install composer`)

## About This Repository

This codebase is maintained in a minimal working repository containing custom code
and manifests of upstream dependencies. [Grunt-Drupal-Tasks](https://github.com/phase2/grunt-drupal-tasks)
(a Node.js tool based on the popular Grunt task-runner) is used for development
and operational management of the application code. A build process downloads and assembles all dependencies for deployment into the webserver.

## Installation

* **`npm install`:** Retrieve build system dependencies.
* **`grunt`:** Validate and assemble functional Drupal codebase.

## Get Oriented

* To learn about available commands run `grunt help`.

## Running on Docker

Running this project on Docker you have streamlined installation steps. The Docker configuration in this repository handles all necessary environment setup for local
development or testing environments.

Local environments assume the use of Phase2's "DevTools" utility to manage the
filesystem, DNS, and any necessary virtualization. Read more about this in the
[Phase2 DevTools documentation](http://phase2.github.io/devtools/).

### First-time Setup

With a working DevTools installation, you can have a locally-hosted, web-browsable
site instance in just a few minutes with two commands.

```bash
git clone git@bitbucket.org:mdeltito/yo-p2-dronetech/yo-p2-drone
bin/start.sh
```

You can use the `start.sh` script later if you want, but please review to ensure
familiarity with the underlying operations.

### Common Operations

These operations are for local development.

* **Start Containers:** `docker-compose up`
* **Build the Site:** `docker-compose -f build.yml run grunt`
* **Open an Interactive Terminal Session:** `docker-compose -f build.yml run cli`
* **Run Drush Command:** `docker-compose -f build.yml run drush <command>`
* **Run Grunt Command:** `docker-compose -f build.yml run grunt [<command>]`

### Services

* **Website:** [http://www.yo-p2-drone.vm](http://www.yo-p2-drone.vm)
* **Database:** `db.yo-p2-drone.vm`

## Further Reading

See `CONTRIBUTING.md` and `JENKINS.md` in this repository for more details on
development processes and expectations.

## Scaffolded with Generators

Initial generation of this project's code structure was built with [Yo P2](https://bitbucket.org/phase2tech/generator-p2)
and related code-generation projects.

To refresh your project with our latest practices, update your local copy of this
tool, and run `yo p2 --replay --force`. Do not forgot to carefully inspect each
change for compatibility with your ongoing project before committing.

The `--replay` option pulls configuration values for the generator from *.yo-rc.json*,
where they can be reviewed or edited by experts at any time.
