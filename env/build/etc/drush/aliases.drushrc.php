<?php
/**
 * Drush Aliases
 *
 * This file provides for the automatic generation of site aliases based
 * on the file layout and configuration of the Docker hosting environment.
 *
 * Site alias for tuned Drush usage with the 'yo-p2-drone' site.
 */

$host = getenv('APP_DOMAIN');

$aliases['yo-p2-drone'] = array(
  'uri' => $host ? $host : 'http://www.yo-p2-drone.vm/',
  'root' => '/var/www/build/html',
  'path-aliases' => array(
    '%drush' => '/var/www/vendor/drush/drush',
    '%drush-script' => '/var/www/vendor/bin/drush',
  ),
);
